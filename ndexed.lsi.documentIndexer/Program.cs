﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ndexed.documents.couchDb.Repository;
using ndexed.documents.elasticSearch.TypeRepository;
using ndexed.lsi.Structures;

namespace ndexed.lsi.documentIndexer
{
    internal class Program
    {
        private static readonly ElasticDocumentTypeRepository m_Repository = new ElasticDocumentTypeRepository();

        private static IDictionary<Guid, svdIndex> m_Indexes = new Dictionary<Guid, svdIndex>();
        private static IDictionary<Guid, DocumentCluster> m_DocumentClusters = new Dictionary<Guid, DocumentCluster>();
        private static List<DocumentCluster> m_Clusters = new List<DocumentCluster>();
        private static CouchDbClusterRepository m_ClusterRepository = new CouchDbClusterRepository();
        private static CouchDbDocumentRankRepository m_RankRepository = new CouchDbDocumentRankRepository();

        private static List<Document> m_DiscardedDocuments = new List<Document>();

        private const int MAX_CLUSTER_SIZE = 800;
        private const int INITIAL_DOCUMENT_SAMPLE_SIZE = 100;
        private const int CLUSTER_COUNT = 5;
        private const int DOCUMENTS_PER_PASS = 10;
        private const int TOTAL_DOCUMENTS_TO_INDEX = 1500;

        private static void Main(string[] args)
        {
            DateTime starTime = DateTime.Now;

            try
            {
                FoldIndexes();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            DateTime endTime = DateTime.Now;
            Console.WriteLine("Total Minutes: {0}", (endTime - starTime).TotalMinutes);
            Console.ReadLine();
        }

        private static void FoldIndexes()
        {
            var documents = m_Repository.GetAll().ToList();


            var tasks = new List<Task>();

            for (var documentIndex = 0; documentIndex < documents.Count; documentIndex += MAX_CLUSTER_SIZE)
            {
                Console.WriteLine("Creating cluster {0}", documentIndex/MAX_CLUSTER_SIZE);

                var current = documents.Skip(documentIndex).Take(MAX_CLUSTER_SIZE);
                var task = new Task(() =>
                {
                    var cluster = new DocumentCluster
                    {
                        ClusterId = Guid.NewGuid(),
                        Documents = current.ToList()
                    };

                    var index = filteredIndexer.createIndex(cluster.Documents);

                    m_Clusters.Add(cluster);
                    m_Indexes.Add(cluster.ClusterId, index);
                });

                tasks.Add(task);
                task.Start();
            }
            if (tasks.Any())
            {
                var finalTask = Task.Factory.ContinueWhenAll(tasks.ToArray(),
                    results => Console.WriteLine("All Clusters Created"));
                finalTask.Wait();
            }

            tasks = new List<Task>();
            foreach (var document in documents)
            {
                var documentToIndex = new Document
                {
                    Id = document.Id,
                    Terms = document.Terms
                };
                //var task = new Task(() =>
                //{
                Console.WriteLine("Indexing Document {0}", documentToIndex.Id);

                var similarRankings = new List<Tuple<Guid, double>>();
                foreach (var index in m_Indexes)
                {
                    var rankings = filteredIndexer.rankDocuments(documentToIndex, index.Value).ToList();
                    similarRankings.AddRange(rankings.Where(item => item.Item2 >= .900).ToList());
                }

                similarRankings = similarRankings.OrderByDescending(rank => rank.Item2).Take(MAX_CLUSTER_SIZE).ToList();

                var documentRanking = new DocumentRank
                {
                    DocumentId = documentToIndex.Id,
                    Rankings = new List<Document>()
                };

                var rankingTasks = new List<Task>();
                foreach (var ranking in similarRankings)
                {
                    var rank = ranking;
                    //var rankingTask = new Task(() =>
                    //{
                        try
                        {
                            var item1 = documents.First(doc => doc.Id == ranking.Item1);
                            var rankDocument = new Document
                            {
                                Id = item1.Id,
                                ContentType = item1.ContentType,
                                Name = item1.Name,
                                PhysicalLocation = item1.PhysicalLocation,
                                Rank = rank.Item2
                            };
                            documentRanking.Rankings.Add(rankDocument);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }
                  //  });

                    //rankingTasks.Add(rankingTask);
                    //rankingTask.Start();
                }

                if (rankingTasks.Any())
                {
                    var finalRankingTasks = Task.Factory.ContinueWhenAll(rankingTasks.ToArray(),
                        results =>
                        {
                            Console.WriteLine("All Documents Ranked");
                            m_RankRepository.Add(documentRanking);
                        });

                    finalRankingTasks.Wait();
                }
                else
                {
                    m_RankRepository.Add(documentRanking);
                }

                Console.WriteLine("Finished Indexing Document {0}", documentToIndex.Id);
            }

            //});

            //tasks.Add(task);
            //task.Start();

            if (tasks.Any())
            {
                var finalTask = Task.Factory.ContinueWhenAll(tasks.ToArray(),
                    results => Console.WriteLine("All Documents Indexed"));
                finalTask.Wait();
            }
        }

        private static Document PlaceDocument(Document documentToPlace, List<Document> documents)
        {
            var clusterRankings = m_Clusters.Select(cluster => GetClusterRanking(documentToPlace, cluster)).ToList();

            var highestRankingCluster = GetHighestRankingCluster(clusterRankings);
            highestRankingCluster.Documents.Add(documentToPlace);
            var refinedIndex = filteredIndexer.createIndex(highestRankingCluster.Documents);
            var documentRankings = new Dictionary<Guid, double>();
            foreach (var clusterDocument in highestRankingCluster.Documents)
            {
                var ranking = filteredIndexer.rankDocuments(clusterDocument, refinedIndex).Sum(rank => rank.Item2);
                documentRankings.Add(clusterDocument.Id, ranking);
            }

            var orderedRankings = documentRankings.OrderByDescending(item => item.Value);
            var itemsToTake = orderedRankings.Count() >= MAX_CLUSTER_SIZE ? MAX_CLUSTER_SIZE : orderedRankings.Count();

            var topTen = orderedRankings.Take(itemsToTake).Select(item => item.Key);
            highestRankingCluster.Documents =
                highestRankingCluster.Documents.Where(doc => topTen.Contains(doc.Id)).ToList();

            Document discardDocument = null;
            if (itemsToTake == MAX_CLUSTER_SIZE)
            {
                var discardId = orderedRankings.Last().Key;
                discardDocument = documents.First(doc => doc.Id == discardId);
                if (discardDocument.Id != documentToPlace.Id)
                {
                    m_Indexes[highestRankingCluster.ClusterId] = filteredIndexer.createIndex(highestRankingCluster.Documents);
                }
            }

            return discardDocument;
        }

        private static void IndexDocuments()
        {
            Console.WriteLine("Getting documents");
            var documents = m_Repository.GetAll().ToArray().OrderByDescending(doc => doc.Terms.Count).ToList();

            m_Indexes = new Dictionary<Guid, svdIndex>();

            DateTime startTime = DateTime.Now;
            m_Clusters = m_ClusterRepository.GetAll().ToList();
            var tasks = new List<Task>();

            var indexedDocumentIds = new List<Guid>();
            Console.WriteLine("Calculating Clusters");
            foreach (var cluster in m_Clusters)
            {
                Console.WriteLine("Cluster {0} started at {1}", cluster.ClusterId, DateTime.Now);
                var clusterId = cluster.ClusterId;
                var docIds = cluster.Documents.Select(doc => doc.Id).ToList();
                indexedDocumentIds.AddRange(docIds);

                var docs = documents.Where(doc => docIds.Contains(doc.Id));

                var indexTask = new Task(() =>
                {
                    var index = filteredIndexer.createIndex(docs);
                    var existingCluster = m_ClusterRepository.Get(clusterId);
                    existingCluster.SerealizedIndexData = null;
                    m_ClusterRepository.Add(existingCluster);

                    m_Indexes[clusterId] = index;
                });

                indexTask.ContinueWith(
                    result => Console.WriteLine("Cluster {0} completed at {1}", clusterId, DateTime.Now));
                tasks.Add(indexTask);
                indexTask.Start();
            }

            if (tasks.Any())
            {
                var finalTask = Task.Factory.ContinueWhenAll(tasks.ToArray(),
                    results => Console.WriteLine("All clusters calculated"));
                finalTask.Wait();
            }

            tasks = new List<Task>();
            foreach (var document in documents)
            {
                Console.WriteLine("Indexing document {0}", document.Id);

                try
                {
                    var documentToIndex = new Document
                    {
                        Id = document.Id,
                        Terms = document.Terms
                    };
                    //var documentTask = new Task(() =>
                    //{
                        try
                        {
                            var documentRankings = new List<Document>();

                            foreach (var documentCluster in m_Clusters)
                            {
                                var rankings = filteredIndexer.rankDocuments(documentToIndex, m_Indexes[documentCluster.ClusterId]).ToList();
                                var rankedDocuments = rankings.Select(ranking => new Document {Id = ranking.Item1, Rank = ranking.Item2});
                                documentRankings.AddRange(rankedDocuments);
                            }

                            var documentRanking = new DocumentRank
                            {
                                DocumentId = documentToIndex.Id,
                                Rankings = new List<Document>()
                            };
                            foreach (var ranking in documentRankings)
                            {
                                var item1 = documents.First(doc => doc.Id == ranking.Id);
                                var rankDocument = new Document
                                {
                                    Id = item1.Id,
                                    ContentType = item1.ContentType,
                                    Name = item1.Name,
                                    PhysicalLocation = item1.PhysicalLocation,
                                    Rank = ranking.Rank
                                };
                                documentRanking.Rankings.Add(rankDocument);
                            }
                            m_RankRepository.Add(documentRanking);
                        }
                        catch (OutOfMemoryException)
                        {
                            //ignore
                        }
                    //});

                    //tasks.Add(documentTask);
                    //documentTask.Start();
                }
                catch (OutOfMemoryException)
                {
                    
                }
                

            }

            if (tasks.Any())
            {
                var finalTask = Task.Factory.ContinueWhenAll(tasks.ToArray(),
                    results =>
                    {
                        var endTime = DateTime.Now;

                        var timespan = endTime - startTime;
                        Console.WriteLine("Total Minutes: {0}", timespan.TotalMinutes);
                    });
                finalTask.Wait();
            }
        }


        static void IndexClusters()
        {
            DateTime startTime = DateTime.Now;
            var documents = m_Repository.GetAll().ToArray().ToList();

            var clusterCount = documents.Count / MAX_CLUSTER_SIZE;

            var initialDocuments = documents.Take(INITIAL_DOCUMENT_SAMPLE_SIZE).ToList();

            InitializeClusters(initialDocuments);

            var remainingDocuments = documents.Skip(INITIAL_DOCUMENT_SAMPLE_SIZE).Take(clusterCount).ToList();

            var remainingPasses = ((documents.Count - clusterCount) / clusterCount);
            for (var index = 1; index < remainingPasses; index++)
            {
                RefineClusters(remainingDocuments);
                remainingDocuments = documents.Skip(clusterCount * index).Take(clusterCount).ToList();
                foreach (var documentCluster in m_Clusters)
                {
                    Console.WriteLine("Cluster {0} Document Count: {1}", documentCluster.ClusterId, documentCluster.Documents.Count);
                }
            }

            DateTime endTime = DateTime.Now;

            var timespan = endTime - startTime;
            Console.WriteLine("Total Minutes: {0}", timespan.TotalMinutes);
            Console.ReadLine();
        }

        static void InitializeClusters(IList<Document> documents)
        {
            Console.WriteLine("Initializing  Clusters");
            
            var tasks = new List<Task>();

            for (var documentIndex = 0; documentIndex < documents.Count; documentIndex += DOCUMENTS_PER_PASS)
            {
                try
                {
                    var cluster = new DocumentCluster
                    {
                        ClusterId = Guid.NewGuid(),
                        Documents = documents.Skip(documentIndex).Take(DOCUMENTS_PER_PASS).ToList()
                    };

                    m_Clusters.Add(cluster);

                    var indexTask = new Task(() =>
                    {
                        var index = filteredIndexer.createIndex(cluster.Documents);
                        m_Indexes.Add(cluster.ClusterId, index);
                    });

                    tasks.Add(indexTask);
                    indexTask.Start();
                   
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }

            if (tasks.Any())
            {
                var finalTask = Task.Factory.ContinueWhenAll(tasks.ToArray(),
                    results => Console.WriteLine("All clusters created"));
                finalTask.Wait();
            }
        }

        private static void CreateSingleCluster()
        {
            DateTime starTime = DateTime.Now;

            var documents = m_Repository.GetSome(TOTAL_DOCUMENTS_TO_INDEX).ToList();
            Console.WriteLine("Creating single cluster");
            var index = filteredIndexer.createIndex(documents);
            DateTime endTime = DateTime.Now;
            Console.WriteLine("Total Minutes: {0}", (endTime - starTime).TotalMinutes);

            starTime = DateTime.Now;
            Console.WriteLine("Indexing Documents");
            foreach (var document in documents)
            {
                var documentToIndex = new Document
                {
                    Id = document.Id,
                    Terms = document.Terms
                };

                var documentRankings = new List<Document>();

                var rankings = filteredIndexer.rankDocuments(documentToIndex, index).ToList();
                var rankedDocuments = rankings.Select(ranking => new Document {Id = ranking.Item1, Rank = ranking.Item2});
                documentRankings.AddRange(rankedDocuments);

                var documentRanking = new DocumentRank
                {
                    DocumentId = documentToIndex.Id,
                    Rankings = new List<Document>()
                };
                foreach (var ranking in documentRankings)
                {
                    var item1 = documents.First(doc => doc.Id == ranking.Id);
                    var rankDocument = new Document
                    {
                        Id = item1.Id,
                        ContentType = item1.ContentType,
                        Name = item1.Name,
                        PhysicalLocation = item1.PhysicalLocation,
                        Rank = ranking.Rank
                    };
                    documentRanking.Rankings.Add(rankDocument);
                }
                m_RankRepository.Add(documentRanking);
            }

            Console.WriteLine("All documents indexed");
            endTime = DateTime.Now;
            Console.WriteLine("Total Minutes: {0}", (endTime - starTime).TotalMinutes);
        }

        static void RefineClusters(IList<Document> documents)
        {
            Console.WriteLine("Refining Clusters");

            //find the most closley correlated cluster, add the document
            foreach (var document in documents)
            {
                try
                {
                    var clusterRankings = m_Clusters.Select(cluster => GetClusterRanking(document, cluster)).ToList();

                    var highestRankingCluster = GetHighestRankingCluster(clusterRankings);

                    highestRankingCluster.Documents.Add(document);
                    highestRankingCluster.HasChanges = true;
                    highestRankingCluster.Changes++;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }

            var changedClusters = m_Clusters.Where(clust => clust.HasChanges);
            var tasks = new List<Task>();
            foreach (var changedCluster in changedClusters)
            {
                if (changedCluster.PercentChange > .10)
                {
                    var clusterId = changedCluster.ClusterId;
                    var docs = changedCluster.Documents;
                    changedCluster.Changes = 0;
                    m_ClusterRepository.Add(changedCluster);

                    var indexTask = new Task(() =>
                    {
                        var index = filteredIndexer.createIndex(docs);
                        m_Indexes[clusterId] = index;
                    });

                    tasks.Add(indexTask);
                    indexTask.Start();
                }
            }

            if (tasks.Any())
            {
                var finalTask = Task.Factory.ContinueWhenAll(tasks.ToArray(),
                    results => Console.WriteLine("All clusters refined"));
                finalTask.Wait();
            }
        }

        static Tuple<Guid, float> GetClusterRanking(Document document, DocumentCluster cluster)
        {
            var index = m_Indexes[cluster.ClusterId];

            var ranking = filteredIndexer.rankDocuments(document, index);

            //get average ranking
            var ranks = ranking.Select(item => item.Item2).ToList();

            var numberOfSameDocuments = ranks.Count(rank => Math.Abs(rank - 1.0f) < 0.000000000001f);
            var averageRanking = 0.0f;
            if (numberOfSameDocuments > 0)
            {
                //if the cosine similarity of any documents in the cluster is 1, then it is the same document
                averageRanking = 1.0f;
            }
            else
            {
                averageRanking = (float) (ranks.Aggregate((a, b) => a + b)/ranks.Count);
            }
            var clusterRanking = new Tuple<Guid, float>(cluster.ClusterId, averageRanking);

            return clusterRanking;
        }

        static DocumentCluster GetHighestRankingCluster(IEnumerable<Tuple<Guid, float>> clusterRankings)
        {
            var highestRankingClusterId = clusterRankings.OrderByDescending(item => item.Item2).First().Item1;
            var highestRankingCluster = m_Clusters.First(clust => clust.ClusterId == highestRankingClusterId);

            return highestRankingCluster;
        }
    }
}
