﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ndexed.documents.elasticSearch.TypeRepository;

namespace ndexed.elasticTests
{
    [TestClass]
    public class elasticDocumentTypeRepositoryTests
    {
        [TestMethod]
        public void getAllDocumentTermsTest()
        {
            var repo = new ElasticDocumentTypeRepository();

            IEnumerable<Document> documents = repo.GetAll();
        }

        [TestMethod]
        public void getSingleDocumentTermsTest()
        {
            var repo = new ElasticDocumentTypeRepository();

            List<Document> documents = repo.GetAll().ToList();
            Assert.IsTrue(documents.Any());

            Document document = repo.Get(documents[0].Id);
            Assert.IsNotNull(document);
        }
    }
}
