﻿module matrixOperations

open MathNet.Numerics.LinearAlgebra
open MathNet.Numerics.LinearAlgebra.Double

let buildMatrix (termVectors:float[][]) = 
    let matrix = Matrix.Build.SparseOfColumnArrays termVectors

    matrix

let diagonal(values:float[])  = 
    Matrix.Build.Diagonal(values)

let reductionRate = 2
let dimensionality = 200;
