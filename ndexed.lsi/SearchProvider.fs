﻿module public ndexed.lsi.SearchProvider

open System
open System.Diagnostics

open ndexed.lsi
open ndexed.lsi.Structures

open MathNet.Numerics.LinearAlgebra
open MathNet.Numerics.LinearAlgebra.Double

type public SearchResult = {
    document:document;
    rank:float
}

let Search(term:String, index:svdIndex) = 

    let queryArray =  term.Split(' ')
    let query = queryArray |> Array.toList

    Debug.WriteLine(query)

    Debug.WriteLine(index.termVector)

 
    let queryTermMatch (queryTerm:string) (term:string) = queryTerm = term 
    let queryTermArray = [| for term in index.termVector-> match List.tryFind(queryTermMatch term) query with 
                                                                                                        |None->0.00;
                                                                                                        |Some value->1.00 |]

    Debug.WriteLine(queryTermArray)

    let queryVector =  Vector.Build.DenseOfArray queryTermArray
    let reducedTermVector = queryVector.SubVector(0, Indexer.dimensionality)  
      
    Debug.WriteLine(reducedTermVector)

    let qvp = (index.Uk * index.Sk').LeftMultiply(reducedTermVector)

    Debug.WriteLine(qvp)

    let columns = index.Vtk.EnumerateColumns()

    Debug.WriteLine("Vtk Columns")
    Debug.WriteLine(columns)

    let rankings = [ for column in columns->Indexer.cosineSimilarity qvp column]

    let results = Seq.zip rankings index.documents

    let sortedResults = Seq.sortBy(fun((x:float), (y:document))->(-x), y) results

    sortedResults

    
let SearchByTerms(query:list<string>, index:svdIndex) = 

    let queryTermMatch (queryTerm:string) (term:string) = queryTerm = term 
    let queryTermArray = [| for term in index.termVector-> match List.tryFind(queryTermMatch term) query with 
                                                                                                        |None->0.00;
                                                                                                        |Some value->1.00 |]

    let dimensionality = Math.Min(matrixOperations.dimensionality, queryTermArray.Length)
    let queryVector =  Vector.Build.DenseOfArray queryTermArray
    let reducedTermVector = queryVector.SubVector(0, dimensionality)  

    let qvp = (index.Uk * index.Sk').LeftMultiply(reducedTermVector)

    let columns = index.Vtk.EnumerateColumns()

    let rankings = [ for column in columns->Indexer.cosineSimilarity qvp column]|>Seq.sortBy(~-)

    let convertedRankings = rankings|>Seq.map (fun rank->if Double.IsNaN(rank) then 0.00 else rank)

    let results = Seq.zip (index.documents|>Seq.map(fun elem->elem.id)) convertedRankings

    let sortedResults = Seq.sortBy(fun((x:Guid), (y:float))->x, (-y)) results

    sortedResults
    
