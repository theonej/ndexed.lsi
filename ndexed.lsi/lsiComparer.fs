﻿module ndexed.lsi.lsiRanker

open System

open MathNet.Numerics.LinearAlgebra
open MathNet.Numerics.LinearAlgebra.Double
open FSharp.Data

let isNumeric (item) = fst(Double.TryParse(item))

let termExistsInEntry(term:string, entry:string[]) = match Array.exists(fun (element)->element = term) entry with
                                                        |true->1.00
                                                        |false->0.00

let termCountInEntry(term:string, entry:string[]) = entry |> Array.filter(fun (element)->element = term) |> Array.length |> float

let cosineSimilarity (vector1: Vector<float>, vector2:Vector<float>) = vector1.DotProduct(vector2) / (vector1.Norm(2.) * vector2.Norm(2.))    

let M = Matrix.Build
let V = Vector.Build

let reductionRate = 2

///returns an array of unsorted rankings of the searchTerms compared to the supplied dictionary of terms
let rank(searchTerms:string[], dictionary:string[][])=
         
    //concatenate all unique document terms into a single sequence
    let aggregateTerms = List.concat [for entry in dictionary->[ for term in entry->term]] 
                            |> Seq.distinct //remove duplicates
                            |> Seq.filter(fun item-> not(isNumeric(item)))//remove numeric terms
                           // |> Seq.sortBy(fun item-> -(item.Length))//sort by negative length to sort in descending order

    //reduce the total terms to the length we want for comparison
    let reducedTerms = aggregateTerms |> Seq.take(dictionary.Length)

    //create term X entry matrix (terms are rows, entries are columns)
    let entryTermVectors = [|for term in reducedTerms->[| for entry in dictionary->termCountInEntry(term, entry) |]|]
    
    //create a sparse matrix from the term vectors 
    let entryTermMatrix = M.SparseOfColumnArrays(entryTermVectors)

    //create singular value decomposition of the matrix
    let svd = entryTermMatrix.Svd(true)

    //reduce the resulting matrices into a lower dimensional space
    let U , sigmas, Vt = svd.U, svd.S, svd.VT

    let S = M.Diagonal(sigmas.ToArray())
    let U' = U.SubMatrix(0, U.RowCount, 0, S.ColumnCount)

    let Uk = U.SubMatrix(0, U.RowCount, 0, reductionRate)
    let Sk = S.SubMatrix(0, reductionRate, 0, reductionRate)
    let Vtk = Vt.SubMatrix(0, reductionRate, 0, Vt.ColumnCount)
    let Sk' = Sk.Inverse()

    //create term vector for the search terms
    let searchTermsArray = [| for term in reducedTerms->termCountInEntry(term, searchTerms)|]
    let searchTermsVector = V.DenseOfArray(searchTermsArray)

    //get the query vector product of the search terms multiplied by the reduced dimension state of the svd
    let qvp = (Uk * Sk').LeftMultiply(searchTermsVector)

    //compare the cosine similarity to each of the results for the entries in the dictionary
    let unsortedRankings = [| for column in Vtk.EnumerateColumns()-> cosineSimilarity(qvp, column) |]

    //return the unsorted rankings so that they match the order of the dictionary that was passed in
    //it is the responsibility of the caller to sort the rankings and correlate them with the items
    //which are represented by the supplied dictionary of terms

    unsortedRankings