﻿module ndexed.lsi.filteredIndexer

open System
open System.Diagnostics
open System.Collections.Generic

open ndexed
open ndexed.lsi.Structures
open ndexed.lsi

open MathNet.Numerics.LinearAlgebra
open MathNet.Numerics.LinearAlgebra.Double

open Brahma
open Brahma.OpenCL
open Brahma.OpenCL.Commands
open OpenCL.Net
open Brahma.FSharp.OpenCL.Extensions
open Brahma.FSharp.OpenCL.Core
open Brahma.FSharp.OpenCL.Translator


let calculateIndex(documentData:IEnumerable<Document>)= 
    
    let documents = [for docData in  documentData -> {
                                                                name = docData.Name; 
                                                                id = docData.Id;
                                                                terms = [for term in docData.Terms->{
                                                                                                        value = term.Value; 
                                                                                                        frequency = float term.Frequency;
                                                                                                    }];
                                                              }
                     ]
    //get ragged lists of terms
    let documentTermLists = [for document in documentData->documentTermExtractor.extractFilteredTerms document]

    //reduce dimmensionality
    let dimensionality = Math.Min(matrixOperations.dimensionality, documentTermLists.Length)

    let aggregateReducedTerms = List.concat documentTermLists|>Seq.distinct|>Seq.take dimensionality|>Seq.toList

    let termVectors =  [for termList in documentTermLists->termVectorBuilder.buildTermVector(termList, aggregateReducedTerms, documents)]|>List.toArray
    let matrix = matrixOperations.buildMatrix termVectors
    let svd = matrix.Svd(true)

    let U , sigmas, Vt = svd.U, svd.S, svd.VT

    let S = matrixOperations.diagonal(sigmas.ToArray())
    let U' = U.SubMatrix(0, U.RowCount, 0, S.ColumnCount)

    let Uk = U.SubMatrix(0, U.RowCount, 0, matrixOperations.reductionRate)
    let Sk = S.SubMatrix(0, matrixOperations.reductionRate, 0, matrixOperations.reductionRate)
    let Vtk = Vt.SubMatrix(0, matrixOperations.reductionRate, 0, Vt.ColumnCount)
    let Sk' = Sk.Inverse()

    let documentList = documentData|>Seq.map(fun elem->{
                                                            name= elem.Name;
                                                            id = elem.Id;
                                                            terms = [for term in elem.Terms->{
                                                                                                        value = term.Value; 
                                                                                                        frequency = float term.Frequency;
                                                                                                    }];
                                                        })|>Seq.toList

    let index = {
                    termVector = aggregateReducedTerms;
                    Uk = Uk;
                    Sk' = Sk';
                    Vtk = Vtk;
                    documents=documentList;
            }

    let rankings = [for document in documentList->SearchProvider.SearchByTerms(document.terms|>Seq.map(fun term->term.value)|>Seq.toList, index)]

    let results = Seq.zip (documentList|>Seq.map(fun elem->elem.id)) rankings

    results


let calculateSvd(termVectors:float[][]) = 
    let matrix = matrixOperations.buildMatrix termVectors

    let svd = matrix.Svd(true)

    svd

let createIndex(documentData:IEnumerable<Document>)= 
    let documents = [for docData in  documentData -> {
                                                                name = docData.Name; 
                                                                id = docData.Id;
                                                                terms = [for term in docData.Terms->{
                                                                                                        value = term.Value; 
                                                                                                        frequency = float term.Frequency;
                                                                                                    }];
                                                              }
                     ]
    //get ragged lists of terms
    let documentTermLists = [for document in documentData->documentTermExtractor.extractFilteredTerms document]

    //reduce dimmensionality
    let aggregateTerms = List.concat documentTermLists|>Seq.distinct|>Seq.toList

    let dimensionality = Math.Min((Seq.length aggregateTerms), matrixOperations.dimensionality)

    let aggregateReducedTerms = aggregateTerms|>Seq.take dimensionality|>Seq.toList

    let termVectors =  [for termList in documentTermLists->termVectorBuilder.buildTermVector(termList, aggregateReducedTerms, documents)]|>List.toArray

    let svd = calculateSvd(termVectors)

    let U , sigmas, Vt = svd.U, svd.S, svd.VT

    let S = matrixOperations.diagonal(sigmas.ToArray())
    let U' = U.SubMatrix(0, U.RowCount, 0, S.ColumnCount)

    let Uk = U.SubMatrix(0, U.RowCount, 0, matrixOperations.reductionRate)
    let Sk = S.SubMatrix(0, matrixOperations.reductionRate, 0, matrixOperations.reductionRate)
    let Vtk = Vt.SubMatrix(0, matrixOperations.reductionRate, 0, Vt.ColumnCount)
    let Sk' = Sk.Inverse()

    let documentList = documentData|>Seq.map(fun elem->{
                                                            name= elem.Name;
                                                            id = elem.Id;
                                                            terms = [for term in elem.Terms->{
                                                                                                        value = term.Value; 
                                                                                                        frequency = float term.Frequency;
                                                                                                    }];
                                                        })|>Seq.toList

    let index = {
                    termVector = aggregateReducedTerms;
                    Uk = Uk;
                    Sk' = Sk';
                    Vtk = Vtk;
                    documents=documentList;
            }

    index

    
let rankDocuments (document:Document, index) = 
    let doc = {
        name= document.Name;
        id = document.Id;
        terms = [for term in document.Terms->{
                                            value = term.Value; 
                                            frequency = float term.Frequency;
                                        }];
    }

    let rankings = SearchProvider.SearchByTerms(doc.terms|>Seq.map(fun term->term.value)|>Seq.toList, index)

    rankings
  

    