﻿module public ndexed.lsi.TreeBuilder

open System
open System.Diagnostics

open ndexed.lsi
open ndexed.lsi.Structures

open ndexed
open ndexed.documents.elasticSearch.TypeRepository


type treeBuilder = {
    node:lsiNode;
    documents:list<document>
}

    let elasticRepository = new ElasticDocumentTypeRepository()

    let addDocument (documents:list<document>) (document:document) = List.append [document] documents

    let newNode = {
                        Id = Guid.NewGuid();
                        Documents = List.Empty
                        Child = None;
                    }

    let newChildNode (parentId:Guid) = {
                                            Id = Guid.NewGuid();
                                            Documents = List.Empty
                                            Child = None;
                                        }

    let addChild (currentNode:lsiNode) (child:lsiNode) = {
                                                            Id = currentNode.Id;
                                                            Child = Some child;
                                                            Documents =  currentNode.Documents;
                                                            }

    let appendDocument (currentNode:lsiNode) (document:document) = {
                                                                Id = currentNode.Id;
                                                                Child = currentNode.Child;
                                                                Documents =  addDocument currentNode.Documents document
                                                             }

    
    let rec foldInDocument (state:lsiNode) (document:document) = match state.Documents.Length with
                                                                                            |10-> addChild state (foldInDocument (newChildNode state.Id) document)
                                                                                            |_->(appendDocument state document)

let buildTree():lsiNode = 
    
    
        let documents =  elasticRepository.GetAll()
 
        let documentList = [for document in documents->{
                                                         name = document.Name;
                                                         id = document.Id;
                                                         terms = List.Empty;(*[
                                                                    for term in document.Terms->{
                                                                                                    value = term.Value;
                                                                                                    frequency = float term.Frequency;
                                                                                                }
                                                                 ];*)
                                                        }
                           ]
         
        let node = newNode
        Debug.WriteLine(node.Id)
            

        let tree = documentList|>List.fold(fun (state:lsiNode) (item)->foldInDocument state item) node

        tree

