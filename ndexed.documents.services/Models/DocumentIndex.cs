﻿using System;
using System.Collections.Generic;
using ndexed.lsi.Structures;

namespace ndexed.documents.services.Models
{
    public class DocumentIndex
    {
        public Guid Id { get; set; }
        public List<Document> Documents { get; set; }
        public svdIndex Index { get; set; }

        public DocumentIndex()
        {
            Documents = new List<Document>();
        }
    }
}