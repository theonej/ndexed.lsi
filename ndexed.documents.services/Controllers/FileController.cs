﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using ndexed.documents.elasticSearch.TypeRepository;

namespace ndexed.documents.services.Controllers
{
    public class FileController : ApiController
    {
        private readonly ElasticDocumentTypeRepository m_DocumentRepository;

        public FileController()
        {
            m_DocumentRepository = new ElasticDocumentTypeRepository();
            
        }

        [HttpGet]
        public HttpResponseMessage Get(Guid id)
        {
            var document = m_DocumentRepository.Get(id);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            var buffer = Convert.FromBase64String(document.Base64EncodedData);
            var memoryStream = new MemoryStream(buffer);
            memoryStream.Position = 0;
            response.Content = new StreamContent(memoryStream);
            response.Content.Headers.ContentLength = memoryStream.Length;
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = document.Name
            };
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return response;
        }
    }
}
