﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ndexed.documents.couchDb.Repository;
using ndexed.documents.elasticSearch.TypeRepository;
using ndexed.lsi;
using ndexed.lsi.Structures;
using Newtonsoft.Json.Linq;

namespace ndexed.documents.services.Controllers
{
    public class DocumentController : ApiController
    {
        private readonly ElasticDocumentTypeRepository m_SearchRepository;
        private readonly CouchDbDocumentRankRepository m_RankRepository;

        public DocumentController()
        {
            m_SearchRepository = new ElasticDocumentTypeRepository();
            m_RankRepository = new CouchDbDocumentRankRepository();     
        }

        [HttpGet]
        public HttpResponseMessage Get(Guid id)
        {
            var document = m_SearchRepository.Get(id);
            var documentRanking = m_RankRepository.Get(id);
            documentRanking.Rankings = documentRanking.Rankings.Where(rank=>rank != null).OrderByDescending(ranking => ranking.Rank).ToList();

            var returnValue = new
            {
                document,
                documentRanking
            };

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, returnValue);

            return response;
        }

        [HttpGet]
        public HttpResponseMessage IndexAll()
        {
            var repo = new ElasticDocumentTypeRepository();
            List<Document> documents = repo.GetAll().ToList();

            JObject returnValue = new JObject();

            JArray documentArray = new JArray();
            foreach (var document in documents)
            {
                IEnumerable<Tuple<double, document>> results = null;//DocumentComparer.Compare(document.Id);
                JObject documentObject = new JObject();
                documentObject.Add("Id", document.Id);
                JArray resultArray = new JArray();
                foreach (var result in results)
                {
                    JObject resultObject = new JObject();
                    resultObject.Add("documentId", result.Item2.id);
                    resultObject.Add("rank", result.Item1);
                    resultArray.Add(resultObject);
                }

                documentObject.Add("results", resultArray);

                documentArray.Add(documentObject);
            }

            returnValue.Add("documents", documentArray);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, returnValue);

            return response;
        }
    }
}
