﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

using ndexed.lsi;
using ndexed.lsi.Structures;

namespace ndexed.documents.services.Controllers
{
    public class DocumentTreeController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage Get()
        {
            var returnValue = TreeBuilder.buildTree();

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, returnValue);

            return response;
        }
    }
}
