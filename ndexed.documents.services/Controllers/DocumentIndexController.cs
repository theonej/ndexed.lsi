﻿
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ndexed.documents.elasticSearch.TypeRepository;
using ndexed.documents.services.Managers;
using ndexed.lsi;

namespace ndexed.documents.services.Controllers
{
    public class DocumentIndexController : ApiController
    {
        private readonly DocumentIndexManager m_Manager = new DocumentIndexManager();
        private readonly ElasticDocumentTypeRepository m_Repository = new ElasticDocumentTypeRepository();

        [HttpGet]
        public HttpResponseMessage IndexDocuments()
        {
            var documents = m_Repository.GetAll();
            var index = filteredIndexer.calculateIndex(documents);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, index);

            return response;
        }
    }
}
