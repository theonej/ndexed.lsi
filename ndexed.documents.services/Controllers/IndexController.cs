﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ndexed.dataAccess;
using ndexed.documents.elasticSearch.TypeRepository;
using ndexed.lsi;
using ndexed.lsi.messaging.Commands.Index;
using ndexed.lsi.messaging.Handlers.Index;
using ndexed.lsi.messaging.Interfaces;
using ndexed.lsi.Structures;

namespace ndexed.documents.services.Controllers
{
    public class IndexController : ApiController
    {
        private readonly IRepository<Document> m_Repo;
        private readonly ICommandHandler<CalculateIndexCommand, svdIndex> m_CalculateHandler;

        public IndexController()
        {
            m_Repo = new ElasticDocumentTypeRepository();
            m_CalculateHandler = new IndexCommandHandler(m_Repo);
        }

        [HttpGet]
        public HttpResponseMessage CalculateIndex(Guid documentId)
        {
            var command = new CalculateIndexCommand();

            var index = m_CalculateHandler.Handle(command);

            var document = m_Repo.Get(documentId);

            var ranks = DocumentComparer.Compare(document, index).Select(result => new { rank = result.Item1, name = result.Item2.name });
          //  var ranks = DocumentComparer.GetAverageIndexScore(document, index);

            var response = Request.CreateResponse(HttpStatusCode.OK, ranks);

            return response;
        }
    }
}
