﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

using ndexed.documents.elasticSearch.TypeRepository;

namespace ndexed.documents.services.Controllers
{
    public class DocumentSearchController : ApiController
    {
        private readonly ElasticDocumentTypeRepository m_SearchRepository;

        public DocumentSearchController()
        {
            m_SearchRepository = new ElasticDocumentTypeRepository();
        }

        [HttpGet]
        public HttpResponseMessage Search(string term)
        {
            var documents = m_SearchRepository.Search(term);
            var response = Request.CreateResponse(HttpStatusCode.OK, documents);

            return response;
        }
    }
}
