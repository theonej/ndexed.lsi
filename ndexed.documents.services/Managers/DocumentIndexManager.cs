﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using ndexed.documents.elasticSearch.TypeRepository;
using ndexed.documents.services.Models;
using ndexed.lsi;

namespace ndexed.documents.services.Managers
{
    public class DocumentIndexManager
    {
        private readonly ElasticDocumentTypeRepository m_Repository = new ElasticDocumentTypeRepository();

        public IEnumerable<DocumentIndex> IndexDocuments()
        {
            var documentIndexes = new List<DocumentIndex>();

            var origin = new DocumentIndex();
            documentIndexes.Add(origin);

            IEnumerable<Document> documents = m_Repository.GetAll();

            foreach (Document document in documents)
            {
                if (document.Terms.Count > 0)
                {
                    IndexDocument(document, documentIndexes);
                }
            }

            return documentIndexes;
        }

        #region Private Methods

        private static void IndexDocument(Document document, List<DocumentIndex> documentIndexes)
        {
            for (int indexCount = 0; indexCount < documentIndexes.Count; indexCount++)
            {
                var documentIndex = documentIndexes[indexCount];

                if (documentIndex.Documents.Count == 0)
                {
                    documentIndex.Documents.Add(document);
                }

                if (documentIndex.Documents.Count > 1)
                {
                    var averages = (from indexDocument in documentIndex.Documents
                        let average = DocumentComparer.GetAverageIndexScore(indexDocument, documentIndex.Index)
                        select new Tuple<double, Document>(average, indexDocument)).ToList();

                    double documentScore = DocumentComparer.GetAverageIndexScore(document, documentIndex.Index);
                    averages.Add(new Tuple<double, Document>(documentScore, document));

                    averages.Sort((average1, average2) => average1.Item1.CompareTo(average2.Item1));

                    if (averages.Count > 10)
                    {
                        Document evictee = averages[10].Item2;
                        if (evictee.Id != document.Id)
                        {
                            documentIndex.Documents = averages.GetRange(0, 10).Select(tuple => tuple.Item2).ToList();
                            documentIndex.Index = Indexer.calculateIndex(documentIndex.Documents);
                        }

                        if (indexCount == (documentIndexes.Count - 1))
                        {
                            var last = new DocumentIndex();
                            last.Documents.Add(evictee);
                            documentIndexes.Add(last);
                        }
                        else
                        {
                            /*uncomment to only use remaining indexes  

                        List<DocumentIndex> remainingNodes = documentIndexes.GetRange(indexCount, documentIndexes.Count - indexCount);
                        IndexDocument(evictee, ref remainingNodes);
                        */

                            //reindexes from the beginning
                            IndexDocument(evictee, documentIndexes);

                        }
                    }
                    else
                    {
                        documentIndex.Documents.Add(document);
                        documentIndex.Index = Indexer.calculateIndex(documentIndex.Documents);
                    }
                }
                else
                {
                    documentIndex.Documents.Add(document);
                    documentIndex.Index = Indexer.calculateIndex(documentIndex.Documents);
                }
                //get the average score for each document in the index, then get the average score of the new docuemnt
                //rank by average, take any document over 10 and recursively call IndexDocument
            }
        }

        #endregion
    }
}