﻿using System;
using System.Collections.Generic;

namespace ndexed
{
    public class Document
    {
        public Document()
        {
            Terms = new List<DocumentTerm>();
        }

        public Guid Id { get; set; }
        public Guid OrganizationId { get; set; }
        public Guid CaseId { get; set; }
        public string Name { get; set; }
        public List<DocumentTerm> Terms { get; set; }
        public string Base64EncodedData { get; set; }
        public string ContentType { get; set; }
        public string PhysicalLocation { get; set; }
        public double Rank { get; set; } 
    }
}
