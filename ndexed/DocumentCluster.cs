﻿using System;
using System.Collections.Generic;

namespace ndexed
{
    public class DocumentCluster
    {
        public DocumentCluster()
        {
            Documents = new List<Document>();
        }

        public Guid ClusterId { get; set; }
        public IList<Document> Documents { get; set; }
        public bool HasChanges { get; set; }
        public int Changes { get; set; }

        public float PercentChange
        {
            get
            {
                var percent = 0.00f;
                if (Documents.Count > 0)
                {
                    percent = (float)Changes/Documents.Count;
                }

                return percent;
            }
        }
        public string SerealizedIndexData { get; set; }
    }
}
