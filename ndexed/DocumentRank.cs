﻿using System;
using System.Collections.Generic;

namespace ndexed
{
    public class DocumentRank
    {
        public Guid DocumentId { get; set; }
        public List<Document> Rankings { get; set; } 
    }
}
