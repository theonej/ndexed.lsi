﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ndexed
{
    public class DocumentTerm
    {
        public string Value { get; set; }
        public int Frequency { get; set; }
    }
}
