﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Runtime.Remoting.Contexts;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using ndexed.lsi.messaging.Commands.Documents;
using ndexed.lsi.messaging.Handlers.Documents;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ICSharpCode.SharpZipLib.Tar;
using ICSharpCode.SharpZipLib.GZip;
using System.Configuration;
using System.IO.Compression;
using ICSharpCode.SharpZipLib.Zip;
using System.Xml;
using System.Text;

namespace ndexed.lsi.documentLoader
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IngestPublicPairData();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            Console.WriteLine("Operation completed");
            Console.ReadLine();
        }

        private static void ExtractTar(string tarFileName, string destFolder)
        {
            Console.WriteLine("extracting tar");
            Stream inStream = File.OpenRead(tarFileName);
            using (inStream)
            {
                TarArchive tarArchive = TarArchive.CreateInputTarArchive(inStream);
                tarArchive.ExtractContents(destFolder);
                tarArchive.Close();

                inStream.Close();
            }
            Console.WriteLine("tar extracted");
        }

        private static void IngestPublicPairData()
        {
            var root = @"C:\code\personal\data\uspto\pairbulk-full-20170709-json";
            var directory = new DirectoryInfo(root);
            var files = directory.GetFiles("*.json");
            foreach (var file in files)
            {
                ProcessPublicPairFile(file);
            }
        }

        private static void ProcessPublicPairFile(FileInfo file)
        {
            var streamReader = new StreamReader(file.FullName);
            using (streamReader)
            {
                var reader = new JsonTextReader(streamReader);
                reader.SupportMultipleContent = true;

                var serializer = new JsonSerializer();
                while (reader.Read())
                {
                    if (reader.TokenType == JsonToken.StartObject && reader.Path != "")
                    {
                        dynamic newObject = serializer.Deserialize(reader);
                        newObject.applicationNumber = newObject.applicationDataOrProsecutionHistoryDataOrPatentTermData[0].applicationNumberText.value.ToString();

                        JObject publicationInfo = newObject.applicationDataOrProsecutionHistoryDataOrPatentTermData[2];
                        if (publicationInfo != null)
                        {
                            if (publicationInfo["applicationPublication"] != null)
                            {
                                newObject.applicationUrl = publicationInfo["applicationPublication"]["webURI"];
                                if (newObject.applicationUrl != null && !string.IsNullOrEmpty(newObject.applicationUrl.ToString().Trim()))
                                {
                                    newObject.applicationContents = GetContents(newObject.applicationUrl.ToString().Trim());
                                }
                            }
                            if (publicationInfo["grantPublication"] != null)
                            {
                                newObject.grantUrl = publicationInfo["grantPublication"]["webURI"];
                                if (newObject.grantUrl != null && !string.IsNullOrEmpty(newObject.grantUrl.ToString().Trim()))
                                {
                                    newObject.grantContents = GetContents(newObject.grantUrl.ToString().Trim());
                                }
                            }
                        }

                        AddObject(newObject);

                        Console.WriteLine("Added pattent {0} to index", newObject.applicationNumber);
                    }
                }
            }
        }

        private static void AddObject(dynamic jObject)
        {
            
            var url = string.Format("http://127.0.0.1:9200/ndexed/patents/{0}", jObject.applicationNumber);

            var client = new HttpClient();
            using (client)
            {
                HttpContent content = new StringContent(jObject.ToString());

                var response = client.PutAsync(url, content).Result;
                var responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }
            }
        }
        private static string GetContents(string url)
        {
            var client = new HttpClient();

            try
            {
                using (client)
                {
                    client.DefaultRequestHeaders.Add("Connection", "close");
                    var result = client.GetAsync(url).Result;
                    var responseMessage = result.Content.ReadAsStringAsync().Result;
                    if (!result.IsSuccessStatusCode)
                    {
                        Console.WriteLine(responseMessage);
                        responseMessage = null;
                    }

                    var returnValue = Regex.Replace(responseMessage, "<.*?>", string.Empty);
                    return returnValue;
                }
            }catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return string.Empty;

            }
        }

        private static void PatentGrantMultiPagePDFImages()
        {
            var tarFilePath = @"C:\code\personal\data\uspto\I20170704.tar";
            var root = @"C:\code\personal\data\uspto\I20170704";

           // ExtractTar(tarFilePath, root);

            UnzipAndIngest(root);
        }

        private static void UnzipAndIngest(string path)
        {
            var directory = new DirectoryInfo(path);
            var zipFiles = directory.GetFiles("*.zip");
            Console.WriteLine("Found {0} zip files in {1}", zipFiles.Length, path);

            var fastZip = new FastZip();
            foreach (var zipFile in zipFiles)
            {
                Console.WriteLine("Unzipping {0}", zipFile.Name);

                var destination = Path.Combine(directory.FullName, zipFile.Name.ToLower().Replace(".zip", ""));
                fastZip.ExtractZip(zipFile.FullName, destination, null);

                File.Delete(zipFile.FullName);
            }

            //if any xml files are found, ingest
            var xmlFiles = directory.GetFiles("*.xml");
            foreach (var xmlFile in xmlFiles)
            {
                IngestPatentXml(xmlFile.FullName);
            }

            //if any tif folders are found, create a folder at the image root with the name of the currect 
            //directory, and place the images there
            var tiffFiles = directory.GetFiles("*.tif");
            foreach (var tiffFile in tiffFiles)
            {
                IngestPatentTiff(directory, tiffFile);
            }

            //call recursively for all child directories
            var childDirectories = directory.GetDirectories();
            foreach (var child in childDirectories)
            {
                UnzipAndIngest(child.FullName);
            }
        }

        private static void IngestPatentTiff(DirectoryInfo parentDirectory, FileInfo tiffFile)
        {
            string imageRoot = @"C:\code\personal\data\n-dexed.images.patent";

            var tiffDirectory = Path.Combine(imageRoot, parentDirectory.Name);
            if (!Directory.Exists(tiffDirectory))
            {
                Directory.CreateDirectory(tiffDirectory);
            }

            var destination = Path.Combine(tiffDirectory, tiffFile.Name);
            if (!File.Exists(destination))
            {
                File.Copy(tiffFile.FullName, destination);
            }
            File.Delete(tiffFile.FullName);
        }

        private static void IngestPatentXml(string fileName)
        {
            var file = new FileInfo(fileName);
            var recordName = file.Name.ToLower().Replace(".xml", "");

            var url = string.Format("http://127.0.0.1:9200/ndexed/patents/{0}", recordName);

            var client = new HttpClient();
            using (client)
            {
                try
                {
                    var fileStream = File.OpenText(fileName);
                    using (fileStream)
                    {
                        var settings = new XmlReaderSettings();
                        settings.DtdProcessing = DtdProcessing.Ignore;
                        settings.XmlResolver = null;

                        var reader = XmlReader.Create(fileStream, settings);
                        using (reader)
                        {
                            var xml = new XmlDocument();
                            xml.Load(reader);

                            string jsonString = Newtonsoft.Json.JsonConvert.SerializeXmlNode(xml);
                            jsonString = FixUpJson(jsonString);

                            HttpContent content = new StringContent(jsonString);

                            var response = client.PutAsync(url, content).Result;
                            var responseMessage = response.Content.ReadAsStringAsync().Result;
                            if (!response.IsSuccessStatusCode)
                            {
                                throw new HttpException((int)response.StatusCode, responseMessage);
                            }
                        }

                    }

                    File.Delete(fileName);

                    Console.WriteLine("Uploaded {0}", url);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }

        private static string FixUpJson(string json)
        {
            if(json.IndexOf("abstract") >= 0)
            {
                //stop
            }
            var jObject = JObject.Parse(json);

            var consolidatedObject = new JObject();
            var paths = new string[]
            {   "@lang",
                "@dtd-version",
                "@file",
                "@status",
                "@id",
                "@country",
                "@date-produced",
                "@date-publ",
                "us-bibliographic-data-grant",
                "abstract"
            };

            foreach(var path in paths)
            {
                consolidatedObject[path] = jObject["us-patent-grant"][path];
            }

            var text = GetAllTextNodes(jObject);

            consolidatedObject["text"] = new JArray(text);
            return consolidatedObject.ToString();
        }

        private static string[] GetAllTextNodes(JToken token)
        {
            List<string> text = new List<string>();

            if(token != null)
            {
                var textTokens = token.SelectTokens("#text");
                if (textTokens != null && textTokens.Count() > 0)
                {
                    foreach(var textToken in textTokens)
                    {
                        text.Add(textToken.ToString());
                    }
                }

                if (token.HasValues)
                {
                    foreach(JToken child in token.Children())
                    {
                        var childText = GetAllTextNodes(child);
                        text.AddRange(childText);
                    }
                }
            }

            return text.ToArray();
        }

        private static JToken GetTokenByPath(string[] paths, JObject jObject)
        {
            JToken currentObject = jObject;
            foreach (var path in paths)
            {
                if (currentObject[path] != null)
                {
                    currentObject = currentObject[path];
                }
                else
                {
                    currentObject = null;
                    break;
                }
            }

            return currentObject;
        }

        private static void AddOpinions()
        {
            var directoryPath = @"c:\all\";
            var directory = new DirectoryInfo(directoryPath);
            var files = directory.GetFiles("*.tar.gz");
            foreach (var file in files)
            {
                Console.WriteLine("Unzipping {0}", file.Name);
                var jurisdictionDirectory = UnzipAndUnpack(file.FullName);

                Console.WriteLine("Uploading files from {0}", file.Name);
                UploadFromJson(jurisdictionDirectory, file.Name.Replace(".tar.gz", ""));

                Console.WriteLine("Cleaning up");
                //clean up
                file.Delete();
                if (Directory.Exists(jurisdictionDirectory))
                {
                    Directory.Delete(jurisdictionDirectory, true);
                }

                Console.WriteLine("File {0} ingested", file.Name);
            }
        }
        private static void UploadFromJson(string directoryPath, string jurisdiction)
        {

            var directory = new DirectoryInfo(directoryPath);
            if (directory.Exists)
            {
                var files = directory.GetFiles("*.json");


                var client = new HttpClient();
                using (client)
                {
                    foreach (var file in files)
                    {
                        try
                        {
                            var fileStream = File.OpenText(file.FullName);
                            using (fileStream)
                            {
                                var contents = fileStream.ReadToEnd();
                                var opinion = JObject.Parse(contents);
                                opinion["jurisdiction"] = jurisdiction;
                                HttpContent content = new StringContent(opinion.ToString());

                                var url = string.Format("http://elasticsearch.n-dexed.com:9200/ndexed/opinions/{0}", string.Format("{0}", file.Name.Replace(".json", "")));
                                var response = client.PutAsync(url, content).Result;
                                var responseMessage = response.Content.ReadAsStringAsync().Result;
                                if (!response.IsSuccessStatusCode)
                                {
                                    throw new HttpException((int)response.StatusCode, responseMessage);
                                }
                                Console.WriteLine("Uploaded {0}", url);
                            }
                            File.Delete(file.FullName);

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }
                    }
                }
            }
        }


        private static void DowloadPdfs()
        {
            var urlFormat = @"search?q=filetype%3Apdf&gws_rd=ssl#q=filetype:pdf&start={0}";
            var linkRegex = new Regex(@"^(http:\/\/).(.pdf)$");

            for (var i = 0; i < 500; i++)
            {
                try
                {
                    Console.WriteLine("Getting Next page");
                    var url = string.Format(urlFormat, i);

                    var client = new HttpClient();
                    client.BaseAddress = new Uri(@"https://www.google.com/");

                    var response = client.GetAsync(url).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    var anchorIndex = content.IndexOf(@"http:", System.StringComparison.Ordinal);
                    while (anchorIndex >= 0)
                    {
                        var endIndex = content.IndexOf(".pdf", anchorIndex, System.StringComparison.InvariantCulture);
                        var whiteSpaceIndex = content.IndexOf(' ', anchorIndex);
                        var nextIndex = content.IndexOf(@"http", anchorIndex + 1,
                            System.StringComparison.InvariantCulture);
                        if (endIndex < whiteSpaceIndex && endIndex < nextIndex)
                        {
                            var length = (endIndex - anchorIndex) + 4;
                            var link = content.Substring(anchorIndex, length);

                            var fileNameIndex = link.LastIndexOf(@"/", System.StringComparison.Ordinal);
                            var fileName = link.Substring(fileNameIndex + 1, (link.Length - fileNameIndex) - 1);

                            var filePath = Path.Combine(@"C:\docs\", fileName);

                            var downloader = new WebClient();
                            downloader.DownloadFile(new Uri(link), filePath);
                            Thread.Sleep(100);
                        }
                        Console.WriteLine("getting next document");
                        anchorIndex = content.IndexOf(@"http:", anchorIndex + 1, System.StringComparison.Ordinal);
                    }
                }
                catch (IOException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }

        private static void GetFileSystemDocs()
        {
            var directoryPath = @"C:\docs";

            var directory = new DirectoryInfo(directoryPath);

            IngestFiles(directory);

        }

        private static void ImportCourtListenerDocs()
        {
            var locationFormat = @"https://www.courtlistener.com/api/rest/v3/search/?page={0}&format=json";

            var initialValue = 1;
            var page = 230;
            var offset = initialValue;//increment by 1000 each time it is run
            int count = 0;
            while (count < 3383298)
            {
                try
                {
                    var location = string.Format(locationFormat, page++);

                    var client = new HttpClient();
                    using (client)
                    {
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                            "Basic",
                            Convert.ToBase64String(
                                System.Text.Encoding.ASCII.GetBytes(
                                    string.Format("{0}:{1}", "theonej", "T@1nsDay"))));

                        var response = client.GetAsync(location).Result;
                        var searchResults = response.Content.ReadAsStringAsync().Result;
                        dynamic results = JsonConvert.DeserializeObject(searchResults);
                        var records = results.results;
                        foreach (var record in records)
                        {
                            record["page"] = page;
                            var id = Guid.NewGuid();
                            Console.WriteLine("Importing Opinion {0} ({1})", record, count++);

                            var formatter = new JsonMediaTypeFormatter();
                            var header = new MediaTypeHeaderValue("application/json");
                            HttpContent content = new ObjectContent(typeof(JObject), record, formatter, header);

                            var url = string.Format("http://localhost:9200/ndexed/opinions/{0}", record["cluster_id"]);
                            response = client.PutAsync(url, content).Result;
                            var responseMessage = response.Content.ReadAsStringAsync().Result;
                            if (!response.IsSuccessStatusCode)
                            {
                                throw new HttpException((int)response.StatusCode, responseMessage);
                            }
                        }
                    }
                }
                catch (HttpException ex)
                {
                    Console.WriteLine(ex);
                    Console.WriteLine("Index: {0}", offset);
                }
                offset++;
            }
        }

        private static async void DownloadAllOpinions()
        {
            Console.WriteLine("beginning opinion download");
            var fileLocation = "https://www.courtlistener.com/api/bulk-data/opinions/all.tar";

            var ouputLocation = @"C:\data\allopinions.tar";

            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage response = await client.GetAsync(fileLocation, HttpCompletionOption.ResponseHeadersRead))
                using (Stream streamToReadFrom = await response.Content.ReadAsStreamAsync())
                {
                    using (Stream streamToWriteTo = File.Open(ouputLocation, FileMode.Create))
                    {
                        await streamToReadFrom.CopyToAsync(streamToWriteTo);
                        Console.WriteLine("done writing file");
                    }
                }
            }
        }

        private static string UnzipAndUnpack(string filePath)
        {
            var inputFile = new FileInfo(filePath);
            var destination = inputFile.FullName.Replace(".tar.gz", "");
            if (!Directory.Exists(destination))
            {
                ExtractTGZ(inputFile.FullName, destination);
            }
            return destination;
        }

        private static void ExtractTGZ(string gzArchiveName, string destFolder)
        {

            Stream inStream = File.OpenRead(gzArchiveName);
            Stream gzipStream = new GZipInputStream(inStream);

            TarArchive tarArchive = TarArchive.CreateInputTarArchive(gzipStream);
            tarArchive.ExtractContents(destFolder);
            tarArchive.Close();

            gzipStream.Close();
            inStream.Close();
        }
        private static void IngestFiles(DirectoryInfo directory)
        {

            List<FileInfo> files = directory.GetFiles().ToList();

            foreach (var fileInfo in files)
            {
                try
                {
                    var document = new Document();
                    document.PhysicalLocation = fileInfo.FullName;
                    document.Name = fileInfo.Name;
                    document.ContentType = MimeMapping.GetMimeMapping(document.Name);

                    var command = new AddDocumentCommand();
                    command.DocumentData = document;

                    var handler = new DocumentCommandHandler();
                    var id = handler.Handle(command);

                    Console.WriteLine("Ingested document {0}", id);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
            if (directory.GetDirectories().Length > 0)
            {
                foreach (var childDirectory in directory.GetDirectories())
                {
                    IngestFiles(childDirectory);
                }
            }
        }

        private static void AddRegulations(string rootDirectory)
        {
            var directory = new DirectoryInfo(rootDirectory);

            var children = directory.GetDirectories();
            foreach (var child in children)
            {
                var pdfDirectory = child.GetDirectories().First().GetDirectories("pdf").FirstOrDefault();
                if (pdfDirectory != null)
                {
                    var files = pdfDirectory.GetFiles("*.pdf");

                    foreach (var file in files)
                    {
                        try
                        {
                            Console.WriteLine("Attempting to ingest file {0}", file.FullName);

                            AddDocument(file);

                            Console.WriteLine("File {0} ingested", file.Name);

                            file.Delete();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }
                    }
                }

                Console.WriteLine("Directory {0} ingested", child.Name);
                child.Delete(true);
            }
        }

        private static void AddDocument(FileInfo documentPdf)
        {

            var documentType = 1;

            var encodedData = EncodeDocumentContents(documentPdf.FullName);

            var document = new
            {
                Id = Guid.NewGuid(),
                Name = documentPdf.Name,
                ContentType = MimeMapping.GetMimeMapping(documentPdf.Extension),
                Base64EncodedData = encodedData,
                Size = GetSize(encodedData),
                CreatedDateTime = DateTime.Now,
                Type = documentType
            };

            Add(document);
        }

        private static void Add(dynamic item)
        {
            HttpClient client = InitializeClient();
            using (client)
            {
                var itemObject = JObject.FromObject(item);

                var formatter = new JsonMediaTypeFormatter();
                var header = new MediaTypeHeaderValue("application/json");
                HttpContent content = new ObjectContent(typeof(JObject), itemObject, formatter, header);

                var url = string.Format("{0}/{1}?pipeline=attachment", client.BaseAddress.AbsoluteUri, item.Id);
                var response = client.PutAsync(url, content).Result;
                var responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }
            }
        }

        private static HttpClient InitializeClient()
        {
            string BASE_ADDRESS = ConfigurationManager.AppSettings["ElasticsearchBaseAddress"];

            var client = new HttpClient();

            client.BaseAddress = new Uri(string.Format("{0}/regulation", BASE_ADDRESS));

            return client;
        }

        private string GetDocumentName(string documentName)
        {
            int length = documentName.Length > 50 ? 50 : documentName.Length;
            var name = documentName;
            if (length < documentName.Length)
            {
                var startIndex = documentName.LastIndexOf('.');
                if (startIndex < 0)
                {
                    startIndex = documentName.Length - 4;
                }

                var extension = documentName.Substring(startIndex, documentName.Length - startIndex);
                name = documentName.Substring(0, length);
                name = name + extension;
            }

            return name;
        }

        private int GetDocumentType(dynamic settingsObject, MultipartFormDataStreamProvider provider)
        {
            return 1;
        }

        private static int GetSize(string encodedData)
        {
            var bytes = Convert.FromBase64String(encodedData);

            return bytes.Length;
        }

        private static string EncodeDocumentContents(string documentPath)
        {
            var bytes = File.ReadAllBytes(documentPath);

            var encodedData = Convert.ToBase64String(bytes);
            return encodedData;
        }

        private static MemoryStream ReadChunks(string documentName)
        {
            var byteStream = new MemoryStream();

            string tempPath = Path.GetTempPath();
            var directoryName = Path.Combine(tempPath, documentName);
            var directory = new DirectoryInfo(directoryName);

            FileInfo[] files = directory.GetFiles().OrderBy(file => file.Name).ToArray();
            foreach (var file in files)
            {
                byte[] buffer = File.ReadAllBytes(file.FullName);
                byteStream.Write(buffer, 0, buffer.Length);
                File.Delete(file.FullName);
            }

            return byteStream;
        }
    }
}
