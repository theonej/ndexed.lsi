﻿using System;
using System.Collections.Generic;

namespace ndexed.dataAccess
{
    public interface IRepository<T>
    {
        T Get(Guid id);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetSome(int count);
        void Add(T item);
    }
}
