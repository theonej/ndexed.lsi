﻿using System.Collections.Generic;

namespace ndexed.dataAccess
{
    public interface IQueryRepository<in TQuery, out TResult>
    {
        IEnumerable<TResult> Search(TQuery criteria);
    }
}
