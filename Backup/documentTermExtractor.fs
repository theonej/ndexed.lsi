﻿module documentTermExtractor

open ndexed
open ndexed.lsi.Structures

let extractFilteredTerms(document:Document) = 
    let termList = [for term in document.Terms->term.Value]

    let filteredTerms = termList|> List.filter(termFilter.isWord) 

    filteredTerms
