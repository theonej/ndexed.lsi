﻿module termVectorBuilder

open ndexed.lsi
open ndexed.lsi.Structures

let buildTermVector (documentTerms:list<string>, aggregateTerms:list<string>, documents:list<document>) = 
    let termVector = aggregateTerms|>Seq.map(fun elem->termValueCalculator.calculateTermValue(elem, documentTerms))|>Seq.toArray

    termVector