﻿module public ndexed.lsi.Indexer

open System
open System.Diagnostics
open System.Collections.Generic

open MathNet.Numerics.LinearAlgebra
open MathNet.Numerics.LinearAlgebra.Double
open FSharp.Data
open FSharp.Data.JsonExtensions

open ndexed
open ndexed.lsi.Structures

open ndexed.documents.elasticSearch.TypeRepository

    let reductionRate = 2
    let k = reductionRate
    let dimensionality = 500

    //examines a term list to find the frequency of a given term
    let isMatch (termName:string) (elem:term) = elem.value = termName
    let getTerm (terms:list<term>) (name:string) = List.tryFind (isMatch name) terms
    let localFrequency (terms:list<term>) (name:string) = match (getTerm terms name) with
                                                                                        | None -> 0.00
                                                                                        | Some value->value.frequency

    let localWeight (terms:list<term>) (name:string) = log ((localFrequency terms name) + 1.00)

    //get the occurrance of a term within the entire set of documents
    let globalOccurances (documents:list<document>) (name:string) = [for document in documents->match (getTerm document.terms name) with
                                                                                                                                           | None->{
                                                                                                                                                        value=name;
                                                                                                                                                        frequency = 0.00;
                                                                                                                                            }
                                                                                                                                            |Some value->value]

    let globalFrequency (documents:list<document>) (name:string) = List.fold (fun (count:float) (item:term)->count + item.frequency) 0.00 (globalOccurances documents name)

    let localRatio (documents:list<document>) (terms:list<term>) (name:string) = (localFrequency terms name) / (globalFrequency documents name)


    let printLocalRatio (documents:list<document>) (terms:list<term>) (name:string) = printfn "%f" (globalFrequency documents name)

    let entropy (documents : list<document>)  (terms:list<term>) (name:string) =
        let n = float documents.Length
        let pij = localRatio documents terms name

        Seq.groupBy id documents
        |> Seq.map (fun (_, vals) ->  float (pij * (log pij)) / (log n) )
        |> Seq.fold (fun e p -> e + p) 0.
        |>fun totals -> totals + 1.00

    let globalWeight (documents:list<document>) (terms:list<term>) (name:string)  =  match (entropy documents terms name) with 
                                                                                                                           | nan->1.00
                                                                                                                           |_ ->(entropy documents terms name)                                                                                                          
    let showVectors (vector1: Vector<float>) vector2 = printfn "%A - %A" vector1 vector2
   
    let cosineSimilarity (vector1: Vector<float>) vector2 = vector1.DotProduct(vector2) / (vector1.Norm(2.) * vector2.Norm(2.))
    
    let M = Matrix.Build
    let elasticRepository = new ElasticDocumentTypeRepository()
    
    let removeNumericTerms terms = seq{for n in terms do if not (fst(Double.TryParse(n))) then yield n}

    let termExistsInDocument (terms:list<term>) (name:string) = match List.exists(fun (element:term)->element.value = name) terms with
                                                                                                                             |true->1.00
                                                                                                                             |false->0.00

let calculateIndex(documentData:IEnumerable<Document>)= 

        let documents = [for docData in  documentData -> {
                                                                name = docData.Name; 
                                                                id = docData.Id;
                                                                terms = [for term in docData.Terms->{
                                                                                                        value = term.Value; 
                                                                                                        frequency = float term.Frequency;
                                                                                                    }];
                                                              }
                     ]
        let aggregateTermVector = List.concat [ for document in documentData->[for term in document.Terms->term.Value] ]
        let reducedTermVector = set aggregateTermVector

        let numberReducedTerms = List.ofSeq(removeNumericTerms reducedTermVector)

        //when we truncate, numeric terms like '0.1' will be truncated
        let sortedResults = List.rev numberReducedTerms

        Debug.WriteLine("Starting Matrix Construction: {0}", DateTime.Now);
        //uncomment to use log * entropy
       // let documentTermVector =  [|for document in documents->[|for name in numberReducedTerms->(localWeight document.terms name) *  (globalWeight documents document.terms name)|]|]
        
        let documentTermVectors =  [|for document in documents->[| for name in numberReducedTerms->termExistsInDocument document.terms name |]|]
        Debug.WriteLine(documentTermVectors)

        Debug.WriteLine("Ending Matrix Construction: {0}", DateTime.Now);

        let documentTermMatrix = M.SparseOfColumnArrays documentTermVectors
        
        Debug.WriteLine("Starting SVD: {0}", DateTime.Now);

        let reducedTermMatrix = documentTermMatrix.SubMatrix(0, dimensionality, 0, documentTermMatrix.ColumnCount)
        let svd = reducedTermMatrix.Svd(true)

        Debug.WriteLine("Ending SVD: {0}", DateTime.Now);

        let U , sigmas, Vt = svd.U, svd.S, svd.VT

        let S = M.Diagonal(sigmas.ToArray())
        let U' = U.SubMatrix(0, U.RowCount, 0, S.ColumnCount)

        let Uk = U.SubMatrix(0, U.RowCount, 0, k)
        let Sk = S.SubMatrix(0, k, 0, k)
        let Vtk = Vt.SubMatrix(0, k, 0, Vt.ColumnCount)
        let Sk' = Sk.Inverse()

        {
            termVector = numberReducedTerms;
            Uk = Uk;
            Sk' = Sk';
            Vtk = Vtk;
            documents=documents;
        }
