﻿module termValueCalculator

open System.Diagnostics

open ndexed.lsi
open ndexed.lsi.Structures

let calculateTermValue(term:string, documentTerms:list<string>) = 

    let termValue = match (documentTerms|>List.filter(fun elem->elem = term)|>Seq.length >0) with |true->1.00 |false->0.00
    termValue

let logEntropy(term:string, documentTerms:list<string>, aggregateTerms:list<string>, documents:list<document>) = 

   
    let localFrequency = documentTerms|>Seq.filter(fun element->element = term)|>Seq.length

    let globalFrequency = aggregateTerms|>Seq.filter(fun element->element = term)|>Seq.length

    let localRatio = float (localFrequency / globalFrequency)

    let localWeight = log(float(localFrequency) + 1.00)

    let documentCount = float (documents|>Seq.length)
    let logCount = log(documentCount)
    let logLocal = log(localRatio)

    let entropy = documentTerms|> Seq.map (fun item ->  float ((localRatio * logLocal) / logCount) )|> Seq.fold (fun e p -> e + p) 0.|>fun totals -> totals + 1.00

    entropy
 