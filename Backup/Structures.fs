﻿namespace ndexed.lsi.Structures

open System

open MathNet.Numerics.LinearAlgebra

type term ={
    value:string;
    frequency:float
}

type public document = {
    name:string;
    id:Guid;
    terms:list<term>;
}

type public svdIndex = {
        termVector:List<string>;
        Uk:Matrix<float>;
        Sk':Matrix<float>;
        Vtk:Matrix<float>;
        documents:list<document>
    }

type lsiNode = {
        Id:Guid;
     //   Index:svdIndex;
        Child:lsiNode option;
        Documents:list<document>
    }

