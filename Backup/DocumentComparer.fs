﻿module ndexed.lsi.DocumentComparer

open System
open System.Diagnostics

open ndexed
open ndexed.lsi
open ndexed.lsi.Structures

open MathNet.Numerics.LinearAlgebra
open MathNet.Numerics.LinearAlgebra.Double

type public SearchResult = {
    document:document;
    rank:float
}

let GetAverageIndexScore(document:Document) (index:svdIndex) = 
    let query = [for term in document.Terms->term.Value]
    let queryTermMatch (queryTerm:string) (term:string) = queryTerm = term 
    let queryTermArray = [| for term in index.termVector-> match List.tryFind(queryTermMatch term) query with 
                                                                                                        |None->0.00;
                                                                                                        |Some value->1.00 |]

    Debug.WriteLine(queryTermArray)

    let queryVector =  Vector.Build.DenseOfArray queryTermArray
    let reducedTermVector = queryVector.SubVector(0, Indexer.dimensionality)  
      
    Debug.WriteLine(reducedTermVector)

    let qvp = (index.Uk * index.Sk').LeftMultiply(reducedTermVector)

    let columns = index.Vtk.EnumerateColumns()


    let rankings = [ for column in columns->Indexer.cosineSimilarity qvp column]

    let average = Seq.average rankings

    average

let Compare(document:Document) (index:svdIndex) = 

    Debug.WriteLine(index.termVector)

    let query = [for term in document.Terms->term.Value]

    let queryTermMatch (queryTerm:string) (term:string) = queryTerm = term 
    let queryTermArray = [| for term in index.termVector-> match List.tryFind(queryTermMatch term) query with 
                                                                                                        |None->0.00;
                                                                                                        |Some value->1.00 |]

    Debug.WriteLine(queryTermArray)

    let queryVector =  Vector.Build.DenseOfArray queryTermArray
    let reducedTermVector = queryVector.SubVector(0, Indexer.dimensionality)  
      
    Debug.WriteLine("Reduced term vector length: {0}", reducedTermVector.Count)

    let qvp = (index.Uk * index.Sk').LeftMultiply(reducedTermVector)

    Debug.WriteLine(qvp)

    let columns = index.Vtk.EnumerateColumns()


    let rankings = [ for column in columns->Indexer.cosineSimilarity qvp column]

    let results = Seq.zip rankings index.documents

    let sortedResults = Seq.sortBy(fun((x:float), (y:document))->(-x), y.id) results

    sortedResults
    
