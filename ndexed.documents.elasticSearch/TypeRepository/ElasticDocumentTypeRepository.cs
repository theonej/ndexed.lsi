﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web;
using ndexed.dataAccess;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using HttpClientFactory = ndexed.documents.elasticSearch.Factory.HttpClientFactory;

namespace ndexed.documents.elasticSearch.TypeRepository
{
    public class ElasticDocumentTypeRepository : IRepository<Document>,
                                                 IQueryRepository<string, Document>
    {
        private const string BASE_ADDRESS = "/ndexed/document/";
        private const int ALL_DOCUMENTS_COUNT = 10000;

        public void Add(Document item)
        {
            var client = HttpClientFactory.InitializeClient(typeof(Document));
            using (client)
            {
                string fileContent = item.Base64EncodedData;
                var itemObject = JObject.FromObject(item);

                JObject file = new JObject
                {
                    {"_content_type", item.ContentType},
                    {"_name", item.Name},
                    {"_content", fileContent},
                    {"file", fileContent}
                };
                itemObject.Add("file", file);

                var formatter = new JsonMediaTypeFormatter();
                var header = new MediaTypeHeaderValue("application/json");
                HttpContent content = new ObjectContent(typeof(JObject), itemObject, formatter, header);

                var url = string.Format("{0}{1}", BASE_ADDRESS, item.Id);
                HttpResponseMessage response = client.PutAsync(url, content).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }
            }
        }

        public IEnumerable<Document> GetAll()
        {
            var documents = GetDocuments(ALL_DOCUMENTS_COUNT);

            return documents;
        }

        public Document Get(Guid id)
        {
            var client = HttpClientFactory.InitializeClient(typeof(Document));

            using (client)
            {

                string resourcePath = string.Format("{0}/{1}",BASE_ADDRESS, id);

                HttpResponseMessage response = client.GetAsync(resourcePath).Result;
                string responseContent = response.Content.ReadAsStringAsync().Result;
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new OperationCanceledException(responseContent);
                }
                var doc = (JObject) JsonConvert.DeserializeObject(responseContent);

                var document = JsonConvert.DeserializeObject<Document>(doc["_source"].ToString());

                return document;
            }
        }


        public IEnumerable<Document> GetSome(int count)
        {
            var documents = GetDocuments(count);

            return documents;
        }


        public IEnumerable<Document> Search(string query)
        {
            var returnValue = new List<Document>();

            JObject request = new JObject
            {
                {
                    "fields", new JArray
                    {
                        "ContentType", 
                        "Id",
                        "Name",
                        "Extension",
                        "PhysicalLocation"
                    }
               },
                {
                    "query", new JObject
                    {
                        {
                            "match", new JObject
                            {
                                {"file", string.Format("{0}", query)}
                            }
                        }
                    }
                },
                {
                    "highlight", new JObject
                    {
                        {
                            "fields", new JObject
                            {
                                {"file", new JObject()}
                            }
                        }
                    }
                }
            };

            HttpClient client = HttpClientFactory.InitializeClient(typeof(Document));
            using (client)
            {
                var formatter = new JsonMediaTypeFormatter();
                var header = new MediaTypeHeaderValue("application/json");
                HttpContent content = new ObjectContent(typeof(JObject), request, formatter, header);

                var url = string.Format("{0}_search?size=100", BASE_ADDRESS);
                HttpResponseMessage response = client.PostAsync(url, content).Result;
                string responseMessage = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, responseMessage);
                }

                dynamic results = JsonConvert.DeserializeObject(responseMessage);
                JArray hits = results.hits.hits;
                foreach (JToken hit in hits)
                {

                    var fields = hit["fields"];
                    if (fields != null)
                    {
                        var document = new Document
                        {
                            ContentType = fields["ContentType"].First.ToString(),
                            Id = Guid.Parse(fields["Id"].First.ToString()),
                            Name = fields["Name"].First.ToString(),
                            PhysicalLocation = fields["PhysicalLocation"].First.ToString()
                        };

                        returnValue.Add(document);
                    }
                }

                return returnValue;
            }
        }

        #region Private Methods

        private IEnumerable<Document> GetDocuments(int count)
        {
            var documents = new List<Document>();

            //make two calls, one to get the ids, then one to get the term vectors

            var client = HttpClientFactory.InitializeClient(typeof(Document));

            using (client)
            {
                //get all
                string resourcePath = string.Format("{0}_search?fields=Name,Id&size={1}",BASE_ADDRESS, count);
                HttpResponseMessage response = client.GetAsync(resourcePath).Result;

                string responseContent = response.Content.ReadAsStringAsync().Result;
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new OperationCanceledException(responseContent);
                }

                var responseObjects = (JObject)JsonConvert.DeserializeObject(responseContent);
                JToken hits = responseObjects["hits"]["hits"];
                foreach (JToken hit in hits)
                {

                    var fields = hit["fields"];
                    if (fields != null)
                    {
                        var document = new Document
                        {
                            Id = Guid.Parse(fields["Id"].First.ToString()),
                            Name = fields["Name"].First.ToString()
                        };

                        documents.Add(document);
                    }
                }
                JObject request = new JObject();
                request.Add("ids", new JArray(documents.Select(doc=>doc.Id)));

                HttpContent content = new StringContent(request.ToString());
                resourcePath = string.Format("{0}_mtermvectors?positions=false&offsets=false", BASE_ADDRESS);
                response = client.PostAsync(resourcePath, content).Result;
                responseContent = response.Content.ReadAsStringAsync().Result;
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new OperationCanceledException(responseContent);
                }
                var termVectors = (JObject)JsonConvert.DeserializeObject(responseContent);
                JToken docs = termVectors["docs"];
                foreach (JToken doc in docs)
                {
                    var document = documents.First(d => d.Id == Guid.Parse(doc["_id"].ToString()));
                    
                    foreach (JToken vector in doc["term_vectors"])
                    {
                        var term = vector.First["terms"];
                        foreach (JProperty value in term.Children())
                        {
                            var docTerm = new DocumentTerm
                            {
                                Value = value.Name,
                                Frequency = int.Parse(value.First["term_freq"].ToString())
                            };
                            document.Terms.Add(docTerm);
                        }
                    }
                }
            }

            return documents;
        } 
        #endregion



    }
}
