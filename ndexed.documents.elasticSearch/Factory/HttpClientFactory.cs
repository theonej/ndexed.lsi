﻿
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace ndexed.documents.elasticSearch.Factory
{
    internal class HttpClientFactory
    {
        private static Dictionary<Type, string> m_EndpointDictionary = new Dictionary<Type, string>
        {
            {typeof(Document), "http://localhost:9200/"}
        }; 

        internal static HttpClient InitializeClient(Type type)
        {
            var client = new HttpClient();

            if (m_EndpointDictionary.ContainsKey(type))
            {
                client.BaseAddress = new Uri(m_EndpointDictionary[type]);
            }

            return client;
        }
    }
}
