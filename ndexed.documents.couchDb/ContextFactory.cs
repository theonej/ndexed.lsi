﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ndexed.documents.couchDb.Repository;

namespace ndexed.documents.couchDb
{
    
    internal static class ContextFactory
    {
        internal const string DOCUMENT_CLUSTER_DATABASE = "document_cluster";
        internal const string DOCUMENT_RANKING_DATABASE = "document_ranking";

        internal static HttpClient InitializeClient(string databaseName)
        {
            string baseUrl = ConfigurationManager.AppSettings["documentClusterBaseUrl"];
            if (string.IsNullOrEmpty(baseUrl))
            {
                throw new ConfigurationErrorsException("Base Url for CouchDB not set");
            }

            var client = new HttpClient();
            client.BaseAddress = new Uri(baseUrl);

            bool databaseExists = DatabaseExists(databaseName, client);
            if (! databaseExists)
            {
                CreateDatabase(databaseName, client);
            }
            return client;
        }

        #region Private Methods

        private static bool DatabaseExists(string databaseName, HttpClient client)
        {
            bool databaseExists = true;
            try
            {
                HttpResponseMessage response = client.GetAsync(databaseName).Result;
                response.CheckForError();
            }
            catch (HttpException)
            {
                databaseExists = false;
            }

            return databaseExists;
        }

        private static void CreateDatabase(string databaseName, HttpClient client)
        {
            var content = new StringContent(string.Empty);

            HttpResponseMessage response = client.PutAsync(databaseName, content).Result;
            response.CheckForError();
        }

        #endregion
    }
}
