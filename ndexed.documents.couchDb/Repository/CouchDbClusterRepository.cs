﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ndexed.dataAccess;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ndexed.documents.couchDb.Repository
{
    internal static class HttpExtensions
    {
        internal static void CheckForError(this HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                string errorMessage = response.Content.ReadAsStringAsync().Result;
                throw new HttpException((int)response.StatusCode, errorMessage);
            }
        }
    }

    public class CouchDbClusterRepository : IRepository<DocumentCluster>
    {
        public DocumentCluster Get(Guid id)
        {
            string documentLocation = string.Format("{0}/{1}", ContextFactory.DOCUMENT_CLUSTER_DATABASE, id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.DOCUMENT_CLUSTER_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic itemObject = JsonConvert.DeserializeObject<DocumentCluster>(content);

                return itemObject;
            }
        }

        public void Add(DocumentCluster item)
        {
            var cluster = new DocumentCluster();
            cluster.ClusterId = item.ClusterId;
            cluster.Documents = item.Documents.Select(
                doc => new Document()
                {
                    Name = doc.Name,
                    Id = doc.Id,
                    ContentType = doc.ContentType
                }
                ).ToList();
            cluster.SerealizedIndexData = item.SerealizedIndexData;
            
            var formatter = new JsonMediaTypeFormatter();
            var header = new MediaTypeHeaderValue("application/json");
            HttpContent content = new ObjectContent(typeof(DocumentCluster), cluster, formatter, header);

            string documentLocation = string.Format("{0}/{1}", ContextFactory.DOCUMENT_CLUSTER_DATABASE, cluster.ClusterId);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.DOCUMENT_CLUSTER_DATABASE);
            using (client)
            {
                string revisionNumber = GetDocumentRevisionNumber(cluster.ClusterId, client);
                if (!string.IsNullOrEmpty(revisionNumber))
                {
                    documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                }
                var response = client.PutAsync(documentLocation, content).Result;
                response.CheckForError();
            }
        }


        public IEnumerable<DocumentCluster> GetAll()
        {
            var clusters = new List<DocumentCluster>();

            string documentLocation = string.Format("{0}/_all_docs", ContextFactory.DOCUMENT_CLUSTER_DATABASE);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.DOCUMENT_CLUSTER_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic results = JsonConvert.DeserializeObject(content);
                dynamic rows = new JArray(results.rows);
                foreach (var row in rows)
                {
                    var cluster = Get(Guid.Parse(row.id.ToString()));
                    clusters.Add(cluster);
                }
                return clusters;
            }
        }

        public IEnumerable<DocumentCluster> GetSome(int count)
        {
            throw new NotImplementedException();
        }

        private static string GetDocumentRevisionNumber(Guid id, HttpClient client)
        {
            string revisionNumber = null;
            try
            {
                var documentLocation = string.Format("{0}/{1}", ContextFactory.DOCUMENT_CLUSTER_DATABASE, id);
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                var itemObject = JsonConvert.DeserializeObject<JObject>(content);

                revisionNumber = itemObject["_rev"].ToString();
            }
            catch (HttpException ex)
            {
                if (ex.GetHttpCode() != (int)HttpStatusCode.NotFound)
                {
                    throw;
                }
            }

            return revisionNumber;
        }
    }
}
