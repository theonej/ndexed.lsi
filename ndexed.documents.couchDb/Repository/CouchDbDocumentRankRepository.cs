﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ndexed.dataAccess;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ndexed.documents.couchDb.Repository
{
    public class CouchDbDocumentRankRepository : IRepository<DocumentRank>
    {
        public DocumentRank Get(Guid id)
        {
            string documentLocation = string.Format("{0}/{1}", ContextFactory.DOCUMENT_RANKING_DATABASE, id);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.DOCUMENT_RANKING_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic itemObject = JsonConvert.DeserializeObject<DocumentRank>(content);

                return itemObject;
            }
        }

        public IEnumerable<DocumentRank> GetAll()
        {
            var rankings = new List<DocumentRank>();

            string documentLocation = string.Format("{0}/_all_docs", ContextFactory.DOCUMENT_RANKING_DATABASE);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.DOCUMENT_RANKING_DATABASE);
            using (client)
            {
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                dynamic results = JsonConvert.DeserializeObject(content);
                dynamic rows = new JArray(results.rows);
                foreach (var row in rows)
                {
                    var ranking = Get(Guid.Parse(row.id.ToString()));
                    rankings.Add(ranking);
                }
                return rankings;
            }
        }

        public IEnumerable<DocumentRank> GetSome(int count)
        {
            throw new NotImplementedException();
        }

        public void Add(DocumentRank item)
        {
            var formatter = new JsonMediaTypeFormatter();
            var header = new MediaTypeHeaderValue("application/json");
            HttpContent content = new ObjectContent(typeof(DocumentRank), item, formatter, header);

            string documentLocation = string.Format("{0}/{1}", ContextFactory.DOCUMENT_RANKING_DATABASE, item.DocumentId);

            HttpClient client = ContextFactory.InitializeClient(ContextFactory.DOCUMENT_RANKING_DATABASE);
            using (client)
            {
                string revisionNumber = GetDocumentRevisionNumber(item.DocumentId, client);
                if (!string.IsNullOrEmpty(revisionNumber))
                {
                    documentLocation = string.Format("{0}?rev={1}", documentLocation, revisionNumber);
                }
                var response = client.PutAsync(documentLocation, content).Result;
                response.CheckForError();
            }
        }

        private static string GetDocumentRevisionNumber(Guid id, HttpClient client)
        {
            string revisionNumber = null;
            try
            {
                var documentLocation = string.Format("{0}/{1}", ContextFactory.DOCUMENT_RANKING_DATABASE, id);
                var response = client.GetAsync(documentLocation).Result;
                response.CheckForError();

                var content = response.Content.ReadAsStringAsync().Result;

                var itemObject = JsonConvert.DeserializeObject<JObject>(content);

                revisionNumber = itemObject["_rev"].ToString();
            }
            catch (HttpException ex)
            {
                if (ex.GetHttpCode() != (int)HttpStatusCode.NotFound)
                {
                    throw;
                }
            }

            return revisionNumber;
        }
    }
}
