﻿ndexed.controllers.documents.documentSearchController = function ($scope, documentSearchServices) {
    $scope.searchResults = null;
    $scope.searchCriteria = {
        terms:''
    };

    $scope.search = function() {
        var searchPromise = documentSearchServices.searchDocuments($scope.searchCriteria.terms);

        searchPromise.then($scope.showDocuments).then($scope.showError);
    };

    $scope.showDocuments = function(documents) {
        $scope.searchResults = documents;
    };

    $scope.showError = function(error) {
        console.log(error);
    };
};

ndexed.controller('documentSearchController', ['$scope', 'documentSearchServices', ndexed.controllers.documents.documentSearchController]);