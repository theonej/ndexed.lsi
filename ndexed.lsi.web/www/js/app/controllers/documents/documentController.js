﻿ndexed.controllers.documents.documentController = function ($scope, $routeParams, $sce, documentSearchServices, baseApiUrl) {
    $scope.documentId = $routeParams.documentId;
    $scope.document = null;
    $scope.documentRanking = null;
    $scope.apiUrl = baseApiUrl;

    $scope.initialize = function() {
        var documentPromise = documentSearchServices.getDocumentInfo($scope.documentId);

        documentPromise.then($scope.showDocument, $scope.showError);
    };

    $scope.showDocument = function(documentInfo) {
        $scope.document = documentInfo.document;
        $scope.documentRanking = documentInfo.documentRanking;
        console.log(documentInfo);
    };

    $scope.showError = function (error) {
        console.log(error);
    };

    $scope.getDocumentUrl = function (documentId) {
        var trustedUrl = apiUrl + 'File/' + documentId;
        console.log(trustedUrl);

        return $sce.trustAsResourceUrl(trustedUrl);
    };
};

ndexed.controller('documentController', ['$scope', '$routeParams', '$sce', 'documentSearchServices', 'baseApiUrl', ndexed.controllers.documents.documentController]);