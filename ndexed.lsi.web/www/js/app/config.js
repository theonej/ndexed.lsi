﻿
ndexed.config([
    '$routeProvider',
    '$httpProvider',
    function ($routeProvider, $httpProvider) {

        //routing
        $routeProvider.
             when('/', {
                 templateUrl: 'html/documents/search.html',
                 controller: 'documentSearchController'
             }).
             when('/document/:documentId', {
                 templateUrl: 'html/documents/document.html',
                 controller: 'documentController'
             }).
            otherwise({
                redirectTo: '/'
            });

        //global error handler
        $httpProvider.interceptors.push(['$q', function ($q) {

            return {
                'request': function (config) {
                    return config || $q.when(config);
                },

                'response': function (response) {
                    return response || $q.when(response);
                },

                'responseError': function (rejection) {

                    var result = $q.reject(rejection);
                    return result;
                }

            };

        }]);
    }
]);