﻿ndexed.services.documentSearchServices = function($http, $q, baseApiUrl) {
    return{
        searchDocuments:function(terms) {
            var url = baseApiUrl + 'documentSearch?term=' + terms;

            var headers = {};

            var config = {
                url: url,
                headers: headers
            };

            var deferred = $q.defer();

            $http.get(url, config)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        getDocumentInfo: function (documentId) {
            var url = baseApiUrl + 'document/' + documentId;

            var headers = {};

            var config = {
                url: url,
                headers: headers
            };

            var deferred = $q.defer();

            $http.get(url, config)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    };
};

ndexed.factory('documentSearchServices', ['$http', '$q', 'baseApiUrl', ndexed.services.documentSearchServices]);