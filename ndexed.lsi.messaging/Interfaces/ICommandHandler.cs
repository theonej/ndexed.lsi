﻿namespace ndexed.lsi.messaging.Interfaces
{
    public interface ICommandHandler<in T, out TOut> where T:ICommandInfo
    {
        TOut Handle(T command);
    }
}
