﻿using CuttingEdge.Conditions;

using ndexed.dataAccess;
using ndexed.lsi.messaging.Commands.Index;
using ndexed.lsi.messaging.Interfaces;
using ndexed.lsi.Structures;

namespace ndexed.lsi.messaging.Handlers.Index
{
    public class IndexCommandHandler : ICommandHandler<CalculateIndexCommand, svdIndex>
    {
        private readonly IRepository<Document> m_Repository;

        public IndexCommandHandler(IRepository<Document> repository)
        {
            Condition.Requires(repository).IsNotNull();
            m_Repository = repository;
        }

        public svdIndex Handle(CalculateIndexCommand command)
        {
            var documents = m_Repository.GetAll();

            var index = Indexer.calculateIndex(documents);

            return index;
        }
    }
}
