﻿
using System;
using System.IO;
using ndexed.documents.elasticSearch.TypeRepository;
using ndexed.lsi.messaging.Commands.Documents;
using ndexed.lsi.messaging.Interfaces;

namespace ndexed.lsi.messaging.Handlers.Documents
{
    public class DocumentCommandHandler : ICommandHandler<AddDocumentCommand, Guid>
    {
        private readonly ElasticDocumentTypeRepository m_DocumentRepository;

        public DocumentCommandHandler()
        {
            m_DocumentRepository = new ElasticDocumentTypeRepository();    
        }

        public Guid Handle(AddDocumentCommand command)
        {
            var document = command.DocumentData;
            if (document.Id == Guid.Empty)
            {
                document.Id = Guid.NewGuid();
            }
            byte[] buffer = File.ReadAllBytes(document.PhysicalLocation);
            document.Base64EncodedData = Convert.ToBase64String(buffer);

            m_DocumentRepository.Add(document);

            return document.Id;
        }


    }
}
