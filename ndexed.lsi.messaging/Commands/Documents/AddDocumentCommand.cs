﻿using ndexed;
using ndexed.lsi.messaging.Interfaces;

namespace ndexed.lsi.messaging.Commands.Documents
{
    public class AddDocumentCommand  : ICommandInfo
    {
        public Document DocumentData { get; set; }
    }
}
